﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using WEBAPIDemo.Dtos;
using WEBAPIDemo.Interfaces;

namespace WEBAPIDemo.Handlers
{
    public class ProductHandler : IProductHandler
    {
        private static readonly Product[] Products = new Product[]
{
            new Product
            {
                Id = Guid.NewGuid(),
                Name = "Product1",
                Description = "Product1: A",
                Price = 1000000,
                ExpiredDate = new DateTime (2025, 12,01)
            },
             new Product
            {
                Id = Guid.NewGuid(),
                Name = "Product2",
                Description = "Product1: B",
                Price = 1000000,
                ExpiredDate = new DateTime (2025, 12,01)
            },
              new Product
            {
                Id = Guid.NewGuid(),
                Name = "Product3",
                Description = "Product1: C",
                Price = 1000000,
                ExpiredDate = new DateTime (2025, 12,01)
            },
              new Product
            {
                Id = Guid.NewGuid(),
                Name = "Product4",
                Description = "Product1: D",
                Price = 1000000,
                ExpiredDate = new DateTime (2025, 12,01)
            }
};

        public IEnumerable<Product> GetProducts(string filter)
        {
            if (!string.IsNullOrEmpty(filter))
                return Products.AsQueryable().Where(filter);
            return Products;
        } 
        public IEnumerable<Product> GetProductByFilter(ProductFilter filter)
        {
            if (!string.IsNullOrEmpty(filter.Name))
                return Products.AsQueryable().Where(x=> x.Name == filter.Name && x.Price == filter.Price);
            return Products;
        }
    }
}
