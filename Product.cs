using System;

namespace WEBAPIDemo
{
    public class Product
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public DateTime ExpiredDate { get; set; }
        public string Description { get; set; }
    }
}
