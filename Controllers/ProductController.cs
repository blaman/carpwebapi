﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using WEBAPIDemo.Dtos;
using WEBAPIDemo.Interfaces;

namespace WEBAPIDemo.Controllers
{
   
    public class ProductController : BaseController
    {

        private readonly ILogger<ProductController> _logger;
        private readonly IProductHandler _productHandler;

        public ProductController(ILogger<ProductController> logger, 
            IProductHandler productHandler)
        {
            _logger = logger;
            _productHandler = productHandler;
        }

        [HttpGet]
        public IEnumerable<Product> GetListData(string filter)
        {
            return _productHandler.GetProducts(filter);
        }
        [HttpPost]
        public IEnumerable<Product> GetListData(ProductFilter filter)
        {
            return _productHandler.GetProductByFilter(filter);
        }
    }
}
