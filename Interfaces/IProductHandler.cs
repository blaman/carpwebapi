﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WEBAPIDemo.Dtos;

namespace WEBAPIDemo.Interfaces
{
    public interface IProductHandler
    {
        IEnumerable<Product> GetProducts(string filter);
        IEnumerable<Product> GetProductByFilter(ProductFilter filter);
    }
}
